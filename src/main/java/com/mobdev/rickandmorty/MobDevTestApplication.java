package com.mobdev.rickandmorty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobDevTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobDevTestApplication.class, args);
	}

}
