package com.mobdev.rickandmorty;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobdev.rickandmorty.dto.ApiCharacterInfo;
import com.mobdev.rickandmorty.service.impl.GetInfoServiceImpl;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
class MobDevTestApplicationTests {
	
	
	@MockBean
	private RestTemplate restTemplate;
	
	@Mock
	private ObjectMapper mapper;
	
	@Autowired
	@InjectMocks
	private GetInfoServiceImpl service;
	
	private String characterApiResponseWithLocation = "{\"id\":1,\"name\":\"Rick Sanchez\",\"status\":\"Alive\",\"species\":\"Human\",\"type\":\"\",\"gender\":\"Male\",\"origin\":{\"name\":\"Earth (C-137)\",\"url\":\"https://rickandmortyapi.com/api/location/1\"},\"location\":{\"name\":\"Earth (Replacement Dimension)\",\"url\":\"https://rickandmortyapi.com/api/location/20\"},\"image\":\"https://rickandmortyapi.com/api/character/avatar/1.jpeg\",\"episode\":[\"https://rickandmortyapi.com/api/episode/1\",\"https://rickandmortyapi.com/api/episode/2\",\"https://rickandmortyapi.com/api/episode/3\",\"https://rickandmortyapi.com/api/episode/4\",\"https://rickandmortyapi.com/api/episode/5\",\"https://rickandmortyapi.com/api/episode/6\",\"https://rickandmortyapi.com/api/episode/7\",\"https://rickandmortyapi.com/api/episode/8\",\"https://rickandmortyapi.com/api/episode/9\",\"https://rickandmortyapi.com/api/episode/10\",\"https://rickandmortyapi.com/api/episode/11\",\"https://rickandmortyapi.com/api/episode/12\",\"https://rickandmortyapi.com/api/episode/13\",\"https://rickandmortyapi.com/api/episode/14\",\"https://rickandmortyapi.com/api/episode/15\",\"https://rickandmortyapi.com/api/episode/16\",\"https://rickandmortyapi.com/api/episode/17\",\"https://rickandmortyapi.com/api/episode/18\",\"https://rickandmortyapi.com/api/episode/19\",\"https://rickandmortyapi.com/api/episode/20\",\"https://rickandmortyapi.com/api/episode/21\",\"https://rickandmortyapi.com/api/episode/22\",\"https://rickandmortyapi.com/api/episode/23\",\"https://rickandmortyapi.com/api/episode/24\",\"https://rickandmortyapi.com/api/episode/25\",\"https://rickandmortyapi.com/api/episode/26\",\"https://rickandmortyapi.com/api/episode/27\",\"https://rickandmortyapi.com/api/episode/28\",\"https://rickandmortyapi.com/api/episode/29\",\"https://rickandmortyapi.com/api/episode/30\",\"https://rickandmortyapi.com/api/episode/31\",\"https://rickandmortyapi.com/api/episode/32\",\"https://rickandmortyapi.com/api/episode/33\",\"https://rickandmortyapi.com/api/episode/34\",\"https://rickandmortyapi.com/api/episode/35\",\"https://rickandmortyapi.com/api/episode/36\",\"https://rickandmortyapi.com/api/episode/37\",\"https://rickandmortyapi.com/api/episode/38\",\"https://rickandmortyapi.com/api/episode/39\",\"https://rickandmortyapi.com/api/episode/40\",\"https://rickandmortyapi.com/api/episode/41\"],\"url\":\"https://rickandmortyapi.com/api/character/1\",\"created\":\"2017-11-04T18:48:46.250Z\"}";
	
	private String characterApiResponseWithOutLocation = "{\"id\":10,\"name\":\"Alan Rails\",\"status\":\"Dead\",\"species\":\"Human\",\"type\":\"Superhuman (Ghost trains summoner)\",\"gender\":\"Male\",\"origin\":{\"name\":\"unknown\",\"url\":\"\"},\"location\":{\"name\":\"Worldender's lair\",\"url\":\"https://rickandmortyapi.com/api/location/4\"},\"image\":\"https://rickandmortyapi.com/api/character/avatar/10.jpeg\",\"episode\":[\"https://rickandmortyapi.com/api/episode/25\"],\"url\":\"https://rickandmortyapi.com/api/character/10\",\"created\":\"2017-11-04T20:19:09.017Z\"}";
	
	private String locationApiResponse = "{\"id\":1,\"name\":\"Earth (C-137)\",\"type\":\"Planet\",\"dimension\":\"Dimension C-137\",\"residents\":[\"https://rickandmortyapi.com/api/character/38\",\"https://rickandmortyapi.com/api/character/45\",\"https://rickandmortyapi.com/api/character/71\",\"https://rickandmortyapi.com/api/character/82\",\"https://rickandmortyapi.com/api/character/83\",\"https://rickandmortyapi.com/api/character/92\",\"https://rickandmortyapi.com/api/character/112\",\"https://rickandmortyapi.com/api/character/114\",\"https://rickandmortyapi.com/api/character/116\",\"https://rickandmortyapi.com/api/character/117\",\"https://rickandmortyapi.com/api/character/120\",\"https://rickandmortyapi.com/api/character/127\",\"https://rickandmortyapi.com/api/character/155\",\"https://rickandmortyapi.com/api/character/169\",\"https://rickandmortyapi.com/api/character/175\",\"https://rickandmortyapi.com/api/character/179\",\"https://rickandmortyapi.com/api/character/186\",\"https://rickandmortyapi.com/api/character/201\",\"https://rickandmortyapi.com/api/character/216\",\"https://rickandmortyapi.com/api/character/239\",\"https://rickandmortyapi.com/api/character/271\",\"https://rickandmortyapi.com/api/character/302\",\"https://rickandmortyapi.com/api/character/303\",\"https://rickandmortyapi.com/api/character/338\",\"https://rickandmortyapi.com/api/character/343\",\"https://rickandmortyapi.com/api/character/356\",\"https://rickandmortyapi.com/api/character/394\"],\"url\":\"https://rickandmortyapi.com/api/location/1\",\"created\":\"2017-11-10T12:42:04.162Z\"}";
	
	private String expectOkWithLocation = "{\"id\":1,\"name\":\"Rick Sanchez\",\"status\":\"Alive\",\"species\":\"Human\",\"type\":\"\",\"episode_count\":41,\"origin\":{\"name\":\"Earth (C-137)\",\"url\":\"https://rickandmortyapi.com/api/location/1\",\"dimension\":\"Dimension C-137\",\"residents\":[\"https://rickandmortyapi.com/api/character/38\",\"https://rickandmortyapi.com/api/character/45\",\"https://rickandmortyapi.com/api/character/71\",\"https://rickandmortyapi.com/api/character/82\",\"https://rickandmortyapi.com/api/character/83\",\"https://rickandmortyapi.com/api/character/92\",\"https://rickandmortyapi.com/api/character/112\",\"https://rickandmortyapi.com/api/character/114\",\"https://rickandmortyapi.com/api/character/116\",\"https://rickandmortyapi.com/api/character/117\",\"https://rickandmortyapi.com/api/character/120\",\"https://rickandmortyapi.com/api/character/127\",\"https://rickandmortyapi.com/api/character/155\",\"https://rickandmortyapi.com/api/character/169\",\"https://rickandmortyapi.com/api/character/175\",\"https://rickandmortyapi.com/api/character/179\",\"https://rickandmortyapi.com/api/character/186\",\"https://rickandmortyapi.com/api/character/201\",\"https://rickandmortyapi.com/api/character/216\",\"https://rickandmortyapi.com/api/character/239\",\"https://rickandmortyapi.com/api/character/271\",\"https://rickandmortyapi.com/api/character/302\",\"https://rickandmortyapi.com/api/character/303\",\"https://rickandmortyapi.com/api/character/338\",\"https://rickandmortyapi.com/api/character/343\",\"https://rickandmortyapi.com/api/character/356\",\"https://rickandmortyapi.com/api/character/394\"]}}";
	
	private String expectOkWithOutLocation = "{\"id\":10,\"name\":\"Alan Rails\",\"status\":\"Dead\",\"species\":\"Human\",\"type\":\"Superhuman (Ghost trains summoner)\",\"episode_count\":1,\"origin\":null}";
	
	private String expectBadRequest = "{\"error\":\"Character not found\"}";
	
	private String badparse = "{\"id\":1,\"name\":\"Rick Sanche";
	
	@Test
	public void testServiceOkWithLocation() {
		ResponseEntity<String> characterResponse = new ResponseEntity<String>(characterApiResponseWithLocation,HttpStatus.OK);
		ResponseEntity<String> locationResponse = new ResponseEntity<String>(locationApiResponse,HttpStatus.OK);
		ResponseEntity<String> expected = new ResponseEntity<String>(expectOkWithLocation,HttpStatus.OK);
		when(restTemplate.exchange(Mockito.anyString(), Mockito.any(HttpMethod.class), Mockito.<HttpEntity<String>> any(), Mockito.<Class<String>> any())).thenReturn(characterResponse, locationResponse);
		ResponseEntity<String>  actual = service.process(1);
		log.info(actual.getBody());
		log.info(Integer.toString(actual.getStatusCodeValue()));
		assertEquals(expected.getBody(), actual.getBody());
		assertEquals(expected.getStatusCode(), actual.getStatusCode());
	}
	
	@Test
	public void testServiceOkWithOutLocation() {
		ResponseEntity<String> characterResponse = new ResponseEntity<String>(characterApiResponseWithOutLocation,HttpStatus.OK);		
		ResponseEntity<String> expected = new ResponseEntity<String>(expectOkWithOutLocation,HttpStatus.OK);
		when(restTemplate.exchange(Mockito.anyString(), Mockito.any(HttpMethod.class), Mockito.<HttpEntity<String>> any(), Mockito.<Class<String>> any())).thenReturn(characterResponse);
		ResponseEntity<String>  actual = service.process(10);	
		log.info(actual.getBody());
		log.info(Integer.toString(actual.getStatusCodeValue()));
		assertEquals(expected.getBody(), actual.getBody());
		assertEquals(expected.getStatusCode(), actual.getStatusCode());
	}
	
	@Test
	public void testServiceNotFound() {
		ResponseEntity<String> characterResponse = new ResponseEntity<String>(expectBadRequest,HttpStatus.NOT_FOUND);		
		ResponseEntity<String> expected = new ResponseEntity<String>(expectBadRequest,HttpStatus.NOT_FOUND);
		when(restTemplate.exchange(Mockito.anyString(), Mockito.any(HttpMethod.class), Mockito.<HttpEntity<String>> any(), Mockito.<Class<String>> any())).thenReturn(characterResponse);
		ResponseEntity<String>  actual = service.process(1000);	
		log.info(actual.getBody());
		log.info(Integer.toString(actual.getStatusCodeValue()));
		assertEquals(expected.getBody(), actual.getBody());
		assertEquals(expected.getStatusCode(), actual.getStatusCode());
	}
	
	@Test
	public void errorParseo() {		
		
		ResponseEntity<String> characterResponse = new ResponseEntity<String>(badparse,HttpStatus.OK);
		ResponseEntity<String> locationResponse = new ResponseEntity<String>(locationApiResponse,HttpStatus.OK);
		ResponseEntity<String> expected = new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		when(restTemplate.exchange(Mockito.anyString(), Mockito.any(HttpMethod.class), Mockito.<HttpEntity<String>> any(), Mockito.<Class<String>> any())).thenReturn(characterResponse, locationResponse);		
		ResponseEntity<String>  actual = service.process(1);		
		assertEquals(expected.getBody(), actual.getBody());
		assertEquals(expected.getStatusCode(), actual.getStatusCode());
	}
	
	

}
