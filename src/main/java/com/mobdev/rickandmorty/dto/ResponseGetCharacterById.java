package com.mobdev.rickandmorty.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseGetCharacterById extends MainInfoCharacter{
	
	private int episode_count;
	private FullOrigin origin;

}
