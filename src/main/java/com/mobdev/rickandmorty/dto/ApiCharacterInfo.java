package com.mobdev.rickandmorty.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiCharacterInfo extends MainInfoCharacter{
	
//	private int id;
//	private String name;
//	private String status;
//	private String species;
//	private String type;
	private String gender;
	private Origin origin;
	private Origin location;
	private String image;
	private List<String> episode;
	private String url;
	private Date created;

}
