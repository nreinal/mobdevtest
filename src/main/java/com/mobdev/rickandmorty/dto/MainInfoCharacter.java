package com.mobdev.rickandmorty.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MainInfoCharacter {
	
	private int id;
	private String name;
	private String status;
	private String species;
	private String type;

}
