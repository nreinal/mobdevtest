package com.mobdev.rickandmorty.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiLocationInfo {
	
	private int id;
	private String name;
	private String type;
	private String dimension;
	private List<String> residents;
	private String url;
	private Date created;

}
