package com.mobdev.rickandmorty.service;

import org.springframework.http.ResponseEntity;

public interface GetInfoService {

	public ResponseEntity<String> process(int id);	
}
