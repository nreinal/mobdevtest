package com.mobdev.rickandmorty.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mobdev.rickandmorty.service.GetInfoService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CharacterController {
	
	@Autowired
	private GetInfoService service;
	
	@GetMapping(value="/character/{id}")
	public ResponseEntity<?> getCharacterById(@PathVariable("id") int id){
		log.info("Inicio de proceso para id: {}", id);
		return service.process(id);
	}

}
