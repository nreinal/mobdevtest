package com.mobdev.rickandmorty.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FullOrigin extends Origin{
	private String dimension;
	private List<String> residents;
}
