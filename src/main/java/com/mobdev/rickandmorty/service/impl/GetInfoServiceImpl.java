package com.mobdev.rickandmorty.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobdev.rickandmorty.dto.ApiCharacterInfo;
import com.mobdev.rickandmorty.dto.ApiLocationInfo;
import com.mobdev.rickandmorty.dto.FullOrigin;
import com.mobdev.rickandmorty.dto.ResponseGetCharacterById;
import com.mobdev.rickandmorty.service.GetInfoService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GetInfoServiceImpl implements GetInfoService{
	
	@Autowired
	private RestTemplate resttemplate;
	
	@Value("${endpoint}")
	private String endpoint;
	
	@Override
	public ResponseEntity<String> process(int id) {
		try {
			String urlCharacterInfo = new StringBuilder(endpoint).append(id).toString();
			ObjectMapper mapper = new ObjectMapper();
			ResponseEntity<String> characterInfoResponse = getInfo(urlCharacterInfo);
			if(characterInfoResponse.getStatusCode().equals(HttpStatus.OK)) {
				ApiCharacterInfo characterInfo = mapper.readValue(characterInfoResponse.getBody(),ApiCharacterInfo.class);
				ApiLocationInfo locationInfo = null;
				if(!characterInfo.getOrigin().getUrl().isEmpty()) {
					locationInfo =  mapper.readValue(getInfo(characterInfo.getOrigin().getUrl()).getBody(),ApiLocationInfo.class);
				}								
				return new ResponseEntity<String>(mapper.writeValueAsString(generateResponse(characterInfo,locationInfo)),HttpStatus.OK);
			}else {
				return characterInfoResponse;
			}
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			log.info("Error en el parseo de la informacion: {}", e.getMessage());
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}		
	
	private ResponseGetCharacterById generateResponse(ApiCharacterInfo characterInfo, ApiLocationInfo locationInfo) {
		ResponseGetCharacterById responseCharacterById = new ResponseGetCharacterById();
		responseCharacterById.setId(characterInfo.getId());
		responseCharacterById.setName(characterInfo.getName());
		responseCharacterById.setStatus(characterInfo.getStatus());
		responseCharacterById.setSpecies(characterInfo.getSpecies());
		responseCharacterById.setType(characterInfo.getType());
		responseCharacterById.setEpisode_count(characterInfo.getEpisode().size());
		if(locationInfo != null) {
			responseCharacterById.setOrigin(new FullOrigin());
			responseCharacterById.getOrigin().setName(locationInfo.getName());
			responseCharacterById.getOrigin().setUrl(locationInfo.getUrl());
			responseCharacterById.getOrigin().setDimension(locationInfo.getDimension());
			responseCharacterById.getOrigin().setResidents(locationInfo.getResidents());			
		}		
		return responseCharacterById;
	}

	
	private ResponseEntity<String> getInfo(String url){		
		try {
			return resttemplate.exchange(url,HttpMethod.GET,null, String.class);
		}
		catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
			log.info("HttpStatus: {}", httpClientOrServerExc.getStatusCode());
			log.info("Message: {}", httpClientOrServerExc.getMessage());
			
			return new ResponseEntity<>(httpClientOrServerExc.getResponseBodyAsString(),httpClientOrServerExc.getStatusCode());
		}		
	}  

}
